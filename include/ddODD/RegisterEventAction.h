#ifndef REGISTEREVENTACTION_HH
#define REGISTEREVENTACTION_HH

#include <G4Types.hh>            // for G4int, G4double
#include <vector>                // for vector
#include "G4UserEventAction.hh"  // for G4UserEventAction
#include "G4Timer.hh"            // for G4Timer
class G4Event;
#include "DDG4/Geant4Handle.h"
#include "DDG4/Geant4Kernel.h"
#include "DDG4/Geant4EventAction.h"

/**
       *  \author  M.Frank
       *  \version 1.0
       *  \ingroup DD4HEP_SIMULATION
       */
       namespace dd4hep{
        namespace sim {
      class registerEventAction: public dd4hep::sim::Geant4EventAction{
      public:
        /// Standard constructor with initializing arguments
        registerEventAction(dd4hep::sim::Geant4Context* c, const std::string& n);
        /// Default destructor
        virtual ~registerEventAction();
        /// begin-of-event callback
        inline virtual void begin(const G4Event*) override;
        /// End-of-event callback
        virtual void end(const G4Event*) override;
        /// begin-of-run callback
        void beginRun(const G4Run*);
        /// End-of-run callback
        void endRun(const G4Run*);
        private:
        /// Timer measurement from Geant4
        G4Timer fTimer;
      };
}
}
#endif /* REGISTEREVENTACTION_HH */
