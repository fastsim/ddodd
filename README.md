# ddODD

This repository contains plugins for simulation with DDG4.

## Getting started

Get all needed packages (Geant4, DD4hep), from:
```
source /cvmfs/sw-nightlies.hsf.org/key4hep/setup.sh
```

Then compile this library that contains few plugins for DDG4 simulation:
```
mkdir build
cmake .. -DCMAKE_INSTALL_PREFIX=../install
make install
source ../install/bin/this_ddodd.sh
```
