#include "ddODD/RegisterParticlesModel.h"
#include "ddODD/RegisterEventInformation.h"
#include "G4EventManager.hh"
#include "G4Event.hh"

dd4hep::sim::RegisterParticlesModel::RegisterParticlesModel(dd4hep::sim::Geant4Context* context, const std::string& nam)
      : Geant4FastSimShowerModel(context, nam)
    {}

     void dd4hep::sim::RegisterParticlesModel::modelShower(const G4FastTrack& aTrack, G4FastStep& aStep) {

      // remove particle from further processing by G4
      aStep.KillPrimaryTrack();
      aStep.SetPrimaryTrackPathLength(0.0);
      G4double energy = aTrack.GetPrimaryTrack()->GetKineticEnergy();
      aStep.SetTotalEnergyDeposited(energy);

      G4ThreeVector position  = aTrack.GetPrimaryTrack()->GetPosition();
      G4double eta = position.eta();
       RegisterEventInformation* info = dynamic_cast<RegisterEventInformation*>(
        G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetUserInformation());
      if(info == nullptr)
        {
          info = new RegisterEventInformation();
          G4EventManager::GetEventManager()->GetNonconstCurrentEvent()->SetUserInformation(info);
        }
        info->AddParticle(aTrack.GetPrimaryTrack()->GetParticleDefinition()->GetPDGEncoding(), energy, eta);
    }


#include <DDG4/Factories.h>
DECLARE_GEANT4ACTION(RegisterParticlesModel)