#include "ddODD/RegisterEventAction.h"
#include "ddODD/RegisterEventInformation.h"
#include "G4AnalysisManager.hh"
#include "DD4hep/InstanceCount.h"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4ThreeVector.hh"
#include "G4PrimaryVertex.hh"
#include "G4PrimaryParticle.hh"

dd4hep::sim::registerEventAction::registerEventAction(dd4hep::sim::Geant4Context* c, const std::string& n):
    dd4hep::sim::Geant4EventAction(c,n)
{dd4hep::InstanceCount::increment(this);}

        /// Default destructor
        dd4hep::sim::registerEventAction::~registerEventAction() {
  dd4hep::InstanceCount::decrement(this);
        }

void dd4hep::sim::registerEventAction::begin(const G4Event* aEvent)
{
  fTimer.Start();
}

void dd4hep::sim::registerEventAction::end(const G4Event* aEvent)
{
  fTimer.Stop();
  // Get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  RegisterEventInformation* info = dynamic_cast<RegisterEventInformation*> (aEvent->GetUserInformation());
  if (info==nullptr)
    return;
  for (int bin = 0; bin < 4; bin++)
  {
    G4int electrons = info->GetElectrons(bin);
    G4int gammas = info->GetGammas(bin);
    G4int muons = info->GetMuons(bin);
    G4int pi0s = info->GetPi0s(bin);
    G4int charged_pions = info->GetChargedPions(bin);
    G4int protons = info->GetProtons(bin);
    G4int neutrons = info->GetNeutrons(bin);
    G4int neutrinos = info->GetNeutrinos(bin);
    G4int others = info->GetOthers(bin);
    G4int all = electrons + gammas + muons + pi0s + charged_pions + protons + neutrons + neutrinos + others;

    analysisManager->FillH1(0+bin*10, all);
    analysisManager->FillH1(1+bin*10, electrons);
    analysisManager->FillH1(2+bin*10, gammas);
    analysisManager->FillH1(3+bin*10, muons);
    analysisManager->FillH1(4+bin*10, pi0s);
    analysisManager->FillH1(5+bin*10, charged_pions);
    analysisManager->FillH1(6+bin*10, protons);
    analysisManager->FillH1(7+bin*10, neutrons);
    analysisManager->FillH1(8+bin*10, neutrinos);
    analysisManager->FillH1(9+bin*10, others);
  }

  auto primaryVertex =
    G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetPrimaryVertex();
  auto primaryParticle   = primaryVertex->GetPrimary(0);
  G4double primaryEnergy = primaryParticle->GetTotalEnergy();
  analysisManager->FillNtupleDColumn(0, 0, primaryEnergy);
  analysisManager->FillNtupleDColumn(0, 1, fTimer.GetRealElapsed());
  analysisManager->AddNtupleRow(0);
}

#include "DDG4/Factories.h"
DECLARE_GEANT4ACTION(registerEventAction)